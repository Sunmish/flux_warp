#! /usr/bin/env python

import sys
import numpy as np

from argparse import ArgumentParser
from astropy.io import fits
from astropy.table import Table
from scipy.optimize import curve_fit

from flux_warp.models import LogX
from flux_warp.plot_results import plot_snr_logx
from flux_warp import CustomFormatter, OFFSET_M, OFFSET_C

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Data():
    """
    """

    def __init__(self, inputfile):
        """
        """

        # TODO keep up to date with flux_warp output
        self.names = "ra,dec,mflux,pflux,ratio,model,residual,snr"
        self.data = np.genfromtxt(inputfile, names=self.names)


    def fit_snr(self):
        """
        """

        self.model = LogX()
        self.popt, _ = curve_fit(self.model.evaluate, 
                                 xdata=self.data["snr"],
                                 ydata=self.data["ratio"],
                                 p0=self.model.p0)
        logger.info("found function: offset = {:.5f}*log(SNR) {:+.5f}".format(
            self.popt[0], self.popt[1]))


    def add_to_header(self, image):
        """
        """

        with fits.open(image, mode="update") as f:
            f[0].header[OFFSET_M] = self.popt[0]
            f[0].header[OFFSET_C] = self.popt[1]
            f.flush()


    def apply_to_catalogue(self, catalogue, outname, localrms_column, 
                           flux_columns=["int_flux", "peak_flux"]):
        """
        """

        table = Table.read(catalogue)
        for col in flux_columns:
            if col in table.colnames:
                new_col_name = col + "_offset"
                new_col = table[col] - self.model.evaluate(table[col]/table[localrms_column],
                            *self.popt)
                table.add_column(new_col, name=new_col_name)

        form = outname.split(".")[-1]
        if form.lower() == "fits":
            form = form
        else:
            form = "votable"
        table.write(outname, overwrite=True, format=form)






def main():
    """
    """

    _description = """
    Correct for CLEAN bias by finding offset as a function of SNR.
    """

    _help = {"image": "Input image.",
             "catalogue": "Input catalogue (output from flux_warp)",
             "c": "Switch to correct catalogue instead of image.",
             "cite": "Show citation information then quit."}

    fmt = lambda prog: CustomFormatter(prog)
    ps = ArgumentParser(description=_description,
                        formatter_class=fmt)

    ps.add_argument("image", type=str, help=_help["image"])
    ps.add_argument("catalogue", type=str, help=_help["catalogue"])
    ps.add_argument("-c", "--correct-catalogue", "--is-catalogue",
                    dest="use_catalogue", action="store_true", help=_help["c"])
    ps.add_argument("--cite", action="store_true", help=_help["cite"])
    ps.add_argument("-l", "--localrms-column", "--localrms-key",
                    "--localrms_column", "--localrms_key",
                    dest="localrms_column", type=str, default="local_rms")
    ps.add_argument("-f", "--flux-keys", "--flux-columns", "--flux_keys",
                    "--flux_columns", dest="flux_columns",
                    type=str, default="int_flux,peak_flux")
    ps.add_argument("-o", "--outname", type=str, default=None)
    ps.add_argument("-p", "--plot", action="store_true")
    ps.add_argument("--nolatex", dest="uselatex", action="store_false")


    args, unknown = ps.parse_known_args()
    if unknown != []:
        logger.warn("provided arguments {} not in current use - ignoring".format(unknown))

    if args.cite:
        print("Please cite 'Duchesne et al. in prep.' if this software is used "
              "for research work. Adding a link to the GitLab repository would "
              "also be appreciated: https://gitlab.com/Sunmish/flux_warp")
        sys.exit(0)

    data = Data(args.catalogue)
    data.fit_snr()

    if not args.use_catalogue:
        data.add_to_header(args.image)
    else:
        if args.outname is None:
            args.outname = args.image
        data.apply_to_catalogue(args.image, args.outname, 
                                localrms_column=args.localrms_column, 
                                flux_columns=args.flux_columns.split(","))

    if args.plot:
        outname = args.catalogue.replace("txt", "pdf")
        plot_snr_logx(data.data["snr"], data.data["ratio"], data.popt, outname, uselatex=args.uselatex)




if __name__ == "__main__":
    main()