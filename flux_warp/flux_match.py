from __future__ import print_function, division

import sys
import numpy as np
from math import ceil
from scipy.stats import norm


from astropy.io import fits
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord, FK5, AltAz
from astropy import units as u
from astropy.time import Time

from flux_warp.models import (cpowerlaw,
                              cpowerlaw_amplitude,
                              powerlaw, 
                              from_index,
                              read_in_source, 
                              Quadratic2D, 
                              Linear2D,
                              Poly1D,
                              Poly2D,
                              LogX,
                              Gaussian2D,
                              Gaussian2D_1amp,
                              SOURCES,
                              TELESCOPES) 


from flux_warp.sparse_grid_interpolator import rbf

from scipy.optimize import curve_fit

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class FluxMatch():
    """Class to store and manipulate data as we go."""


    def __init__(self, offset=False):
        """The FluxMatch class.

        Parameters
        ----------
        offset : bool, optional
            Select True if wanting to calculate flux density offsets instead of
            flux density ratios. [Default False]

        """


        self.offset = offset
        if offset:
            self.zero = 0.
        else:
            self.zero = 1.

        self.dtypes = [("index", np.int32),
                       ("ra", np.float32),
                       ("dec", np.float32),
                       ("pflux", np.float32),
                       ("mflux", np.float32),
                       ("snr", np.float32),
                       ("ratio", np.float32),
                       ("logratio", np.float32),
                       ("model", np.float32),
                       ("logmodel", np.float32),
                       ("residual", np.float32),
                       ("logresidual", np.float32),
                       ]
        self.mdata = np.recarray((0,), dtype=self.dtypes)
        self.tdata = None

        self.all_ratios = []


    def sigma_clip(self, sigma=3):
        """
        """

        avg = np.nanmean(self.mdata.logratio)
        std = np.nanstd(self.mdata.logratio)

        previous_len = len(self.mdata)

        logger.debug("avg: {}".format(avg))
        logger.debug("std: {}".format(std))

        self.mdata = self.mdata[abs(self.mdata.logratio-avg) < sigma*std]

        logger.debug("number clipped for sigma={}: {}".format(sigma,
            abs(len(self.mdata)-previous_len)))


    def sort_by_flux(self):
        """Wrapper for sorting the record array by predicted flux values."""
        self.mdata.sort(axis=0, order="pflux")
        self.mdata = self.mdata[::-1]


    def nsrc_cut(self, nmax):
        """Cut record array to the `nmax` brightest sources."""
        self.sort_by_flux()
        if len(self.mdata) > nmax:
            self.mdata = self.mdata[0:nmax-1]
            logger.info("min flux density: {}".format(min(self.mdata.pflux)))
    

    def get_test_subset(self, fraction=0.1, faint=False):
        """Get a subset of sources for testing.

        Parameters
        ----------
        fraction : float, optional
            Float between 0. and 1. Fraction of calibrator sources selected.
            [Default 0.1]
        faint : bool, optional
            Select if wanting the test sources to be selected from the faint
            end of the calibrator sources. [Default False]

        """

        if faint:
            n = int(len(self.mdata)*fraction)
            tslice = np.asarray(range(len(self.mdata)-n, len(self.mdata), 1))            
        else:
            n  = int(1./fraction)
            tslice = np.asarray(range(0, len(self.mdata), n))

        mask = np.ones(len(self.mdata), bool)
        mask[tslice] = 0

        self.tdata = self.mdata[~mask]
        self.mdata = self.mdata[mask]

        logger.info("number of test sources: {}".format(len(self.tdata)))
        logger.info("number of calibrators:  {}".format(len(self.mdata)))



    def get_ratios(self, table, freq, threshold=0., nsrc_max=100,
                   # Table keys:
                   flux_key="flux",
                   ra_key="ra",
                    dec_key="dec",
                    powerlaw_amplitude=None,
                    powerlaw_index=None,
                    powerlaw_curvature=None,
                    flux0=None,
                    freq0=None,
                    flux0multiplier=1.,
                    alpha0=None,
                    localrms=None,
                    nmeas=1.,
                    nmeas_key="nmeas",
                    exclude_large_sources=False,
                    exclude_extra=None,
                    offset_params=[0., 0.]):
        """
        """

        global SOURCES

        if isinstance(table, str):
            table = fits.open(table)[1].data

        model0 = model1 = model2 = model3 = model4 = False
        less_than_threshold = 0

        if None not in [powerlaw_amplitude, powerlaw_curvature, powerlaw_index]:
            check_keys(table, powerlaw_amplitude, powerlaw_curvature, powerlaw_index)
            logger.info("evaluating models using curved powerlaw model.")
            model0 = True
        if None not in [powerlaw_curvature, powerlaw_index, flux0, freq0]:
            check_keys(table, powerlaw_curvature, powerlaw_index, flux0)
            logger.info("evaluating models using curved powerlaw model from a "
                        "reference frequency.")
            model1 = True
        if None not in [powerlaw_amplitude, powerlaw_index]:
            check_keys(table, powerlaw_amplitude, powerlaw_index)
            logger.info("evaluating models using a generic power law model.")
            model2 = True
        if None not in [powerlaw_index, flux0, freq0]:
            check_keys(table, powerlaw_index, flux0)
            logger.info("evaluating models using a generic power law model from a "
                        "reference frequency.")
            model3 = True
        if None not in [flux0, freq0, alpha0]:
            check_keys(table, flux0)
            logger.info("evaluating models using a generic power law from a "
                        "reference frequency and assumed spectral index.")
            model4 = True
        if not (model0 or model1 or model2 or model3 or model4):
            raise RuntimeError("not enough information to evaluate models!")

        # if not (nmeas_key in table.names):
        #     nmeas_key = None
        #     logger.warning("using any number of measurements.")
        nmeas_key = None


        if not (localrms in table.names):
            localrms = None
            logger.warning("no local rms column in model catalogue.")
        else:
            snr_model = LogX()

        for i in range(len(table)):

            # Check if the required number of measurements were made:
            if nmeas_key is not None:
                if table[nmeas_key][i] < nmeas:
                    logger.debug("skipping {} as it has {} measurements".format(i, table[nmeas_key][i]))
                    continue

            # Exclude sources within a certain radius around certain large sources
            # like the LMC or SMC where we do not have good models. 
            if exclude_large_sources or exclude_extra is not None:
                
                if not exclude_large_sources:
                    SOURCES = []
                if exclude_extra is not None:
                    SOURCES = read_in_source(exclude_extra, SOURCES)

                in_excl = False

                source_coords = SkyCoord(ra=table[ra_key][i], 
                                         dec=table[dec_key][i],
                                         unit=(u.deg, u.deg))

                for excl in SOURCES:
                    sep_excl = excl.coords.separation(source_coords)
                    if sep_excl.value <= excl.radius:
                        logger.debug("skipping {} as it is within the {}".format(i, excl.name))
                        in_excl = True

                if in_excl:
                    continue


            f = np.nan

            if model0:
                # Curved power law from full model components:
                if not np.asarray([np.isnan(table[key][i]) for key in 
                    [powerlaw_index, powerlaw_amplitude, powerlaw_curvature]]).all():

                    f = cpowerlaw(freq, 
                                  a=table[powerlaw_amplitude][i], 
                                  b=table[powerlaw_index][i], 
                                  c=table[powerlaw_curvature][i])

            if model1 and np.isnan(f):
                # Curved power law from reference measurement:
                if not np.asarray([np.isnan(table[key][i]) for key in
                    [powerlaw_index, powerlaw_curvature, flux0]]).all():

                    a = cpowerlaw_amplitude(x0=freq0,
                                            y0=table[flux0][i]*flux0multiplier,
                                            b=table[powerlaw_index][i],
                                            c=table[powerlaw_curvature][i])
                    f = cpowerlaw(freq, 
                                  a=a, 
                                  b=table[powerlaw_index][i], 
                                  c=table[powerlaw_curvature][i])

            if model2 and np.isnan(f):
                # Normal power law from full model components:
                if not np.asarray([np.isnan(table[key][i]) for key in
                    [powerlaw_amplitude, powerlaw_index]]).all():

                    f = cpowerlaw(freq,
                                  a=table[powerlaw_amplitude][i],
                                  b=table[powerlaw_index][i],
                                  c=0.)

            if model3 and np.isnan(f):
                # Normal power law from reference measurement:
                if not np.asarray([np.isnan(table[key][i]) for key in
                    [powerlaw_index, flux0]]).all():

                    a = cpowerlaw_amplitude(x0=freq0,
                                            y0=table[flux0][i]*flux0multiplier,
                                            b=table[powerlaw_index][i],
                                            c=0)
                    f = cpowerlaw(freq,
                                  a=a,
                                  b=table[powerlaw_index][i],
                                  c=0.)

            if model4 and np.isnan(f):
                if not np.isnan(table[flux0][i]):

                    f = from_index(freq, 
                                   x1=freq0,
                                   y1=table[flux0][i]*flux0multiplier,
                                   index=alpha0)

            if np.isnan(f):
                continue

            if f > threshold:

                f_measured = table[flux_key][i]

                if localrms is not None:
                    snr_i = f_measured / table[localrms][i]
                    f_measured -= snr_model.evaluate(snr_i, *offset_params)
                else:
                    snr_i = 1.


                if self.offset:
                    r_i = f_measured - f
                    # do not use log for offsets:
                    l_i = r_i
                else:
                    r_i = f_measured / f
                    l_i = np.log10(r_i)

                if np.all(np.isfinite(np.array([r_i, f, f_measured]))):

                    # this row addition is not particularly fast, but given
                    # the catalogue sizes this should not be a problem.
                    row_i = np.array([(i, table[ra_key][i], table[dec_key][i],
                                       f, f_measured, snr_i, r_i, l_i, 
                                       1., 1., 1., 0.)], 
                                    dtype=self.dtypes)
                    self.mdata = np.resize(self.mdata, 
                        self.mdata.shape[0]+1).view(np.recarray)
                    self.mdata[-1] = row_i

            else:
                less_than_threshold += 1

        logger.debug("Number of sources below threshold: {}".format(less_than_threshold))

    def fit_coordinate(self, fitsimage, order=2, stride=49000000, time=None, 
                       telescope="MWA", weight_order=1.):
        """Fit a polynomial as function of declination or elevation.

        Parameters
        ----------
        fitsimage : str
            Path to a FITS image for array size and WCS information.
        order : int, optional
            Order of 1-D polynomial for fitting. [Default 2]
        stride : int, optional
            Number of pixels to process at once. [Default 225000]
        time : astropy.time.Time, optional
            An astropy.time.Time object of the observation. Required only for 
            elevation fitting.
        telescope : str, optional
            Telescope name from flux_warp.models.TELESCOPES. Required for 
            elevation fitting. [Default 'MWA']

        Returns
        -------
        array
            An array of factors/offsets the same 2-D size as the data in the 
            FITS image. 
        flux_warp.models.Poly1D
            Model object with fitting parameters, for use in plotting.

        """

        ref = fits.open(fitsimage)[0]

        if time is not None:
            logger.info("fitting 1-D polynomial as a function of elevation")
            model = Poly1D(mtype="elevation", order=order)

            coords_radec = FK5(ra=self.mdata.ra*u.deg,
                               dec=self.mdata.dec*u.deg)
            coords_altaz = coords_radec.transform_to(AltAz(obstime=time,
                location=TELESCOPES[telescope]))
            coordinate = coords_altaz.alt.value
        else:
            logger.info("fitting 1-D polynomial as a function of declination")
            model = Poly1D(mtype="declination", order=order)
            coordinate = self.mdata.dec

        popt, _ = curve_fit(model.evaluate, 
                            xdata=coordinate, 
                            ydata=self.mdata.logratio,
                            sigma=1./(self.mdata.snr**weight_order),
                            # sigma=self.mdata.snr,
                            absolute_sigma=True,
                            p0=model.p0,
                            method="lm")
        model.store_params(popt)

        w = WCS(ref.header).celestial
        factors = np.full_like(np.squeeze(ref.data), self.zero)
        indices = np.indices(factors.shape)
        x = indices[0].flatten()
        y = indices[1].flatten()
        n = len(x)

        for i in range(0, n, stride):

            sys.stdout.write(u"\u001b[1000D" + "{:.>6.1f}%".format(100.*i/n))
            sys.stdout.flush()

            # stride used because coordinate transform can become costly:
            r, d = w.all_pix2world(y[i:i+stride], x[i:i+stride], 0)
            if model.mtype == "elevation":
                radec_i = FK5(ra=r*u.deg, dec=d*u.deg)
                altaz_i = radec_i.transform_to(AltAz(
                    obstime=time, location=TELESCOPES[telescope]
                    ))
                coordinate = altaz_i.alt.value
            else:
                coordinate = d

            factors[x[i:i+stride], y[i:i+stride]] = Poly1D.evaluate(coordinate, 
                                                                    *popt)
        if not self.offset:
            factors = 10**factors

        print("")

        return factors, model


    def fit_snr(self, order=2):
        """Fit a polynomial function as a function of SNR."""

        model = LogX()
        popt, pcov = curve_fit(model.evaluate, 
                               xdata=self.mdata.snr,
                               ydata=self.mdata.logratio,
                               method="lm",
                               p0=model.p0)
        perr = np.sqrt(np.diag(pcov))

        self.snr_popt = popt
        self.snr_perr = perr

                               
    def fit_screen(self, fitsimage, screen=Quadratic2D, weight_order=1.,
            nolog=False):
        """Fit a linear or quadratic screen.

        Parameters
        ----------
        fitsimage : str
            Path to a FITS image for array size and WCS information.
        screen : [flux_warp.models.Quadtratic2D or flux_warp.models.Linear2D]
            2-D screen object to use for fitting.

        Returns
        -------
        array
            An array of factors/offsets the same 2-D size as the data in the 
            FITS image. 

        """

        ref = fits.open(fitsimage)[0]

        w = WCS(ref.header).celestial
        y, x = w.all_world2pix(self.mdata.ra, self.mdata.dec, 0)
        x = x.astype("i")
        y = y.astype("i")

        factors = np.full_like(np.squeeze(ref.data), self.zero)
        indices = np.indices(factors.shape)
        xi = indices[0].flatten()
        yi = indices[1].flatten()

        if nolog:
            f = self.mdata.ratio
        else:
            f = self.mdata.logratio

        screen.echo(*screen.p0)

        popt, _ = curve_fit(screen.evaluate,
                            # xdata=np.asarray([x, y]),
                            xdata=(x, y),
                            ydata=f,
                            sigma=1/(self.mdata.snr**weight_order),
                            absolute_sigma=True,
                            p0=screen.p0,
                            # method="trf",
                            maxfev=int(100e3)
        )

        screen.echo(*popt)

        factors[xi, yi] = screen.evaluate((xi, yi), *popt)


        if nolog:
            factors = np.log10(factors)

        if not self.offset:
            factors = 10**factors



        return factors


    def fit_coordinate_screen(self, fitsimage, stride=225000, time=None, 
                              telescope="MWA", screen=Quadratic2D,
                              weight_order=1.):
        """
        """

        ref = fits.open(fitsimage)[0]

        if time is not None:
            logger.info("fitting 1-D polynomial as a function of elevation")

            coords_radec = FK5(ra=self.mdata.ra*u.deg,
                               dec=self.mdata.dec*u.deg)
            coords_altaz = coords_radec.transform_to(AltAz(obstime=time,
                location=TELESCOPES[telescope]))
            coordinate1 = coords_altaz.alt.value
            coordinate2 = coords_altaz.az.value
        else:
            logger.info("fitting 1-D polynomial as a function of declination")
            coordinate1 = self.mdata.dec
            coordinate2 = self.mdata.ra

        popt, _ = curve_fit(screen.evaluate,
                            xdata=np.asarray([coordinate1, coordinate2]),
                            ydata=self.mdata.logratio,
                            sigma=1./(self.mdata.snr**weight_order),
                            absolute_sigma=True,
                            p0=screen.p0,
                            method="lm")

        w = WCS(ref.header).celestial
        factors = np.full_like(np.squeeze(ref.data), self.zero)
        indices = np.indices(factors.shape)
        x = indices[0].flatten()
        y = indices[1].flatten()
        n = len(x)

        for i in range(0, n, stride):
        
            r, d = w.all_pix2world(y[i:i+stride], x[i:i+stride], 0)
            if time is not None:
                radec_i = FK5(ra=r*u.deg, dec=d*u.deg)
                altaz_i = radec_i.transform_to(AltAz(
                    obstime=time, location=TELESCOPES[telescope]
                    ))
                coordinate1 = altaz_i.alt.value
                coordinate2 = altaz_i.az.value
            else:
                coordinate1 = d
                coordinate2 = r

            factors[x[i:i+stride], y[i:i+stride]] = screen.evaluate((coordinate1,
                                                                     coordinate2), 
                                                                    *popt)

        if not self.offset:
            factors = 10**factors

        print("")

        return factors, screen






    def get_map(self, image, method="mean", memfrac=0.5, 
                absmem="all", outname=None, smooth=0.,
                time=None, telescope=None, order=2,
                edge_interp=True):
        """Get map of 'correction factors'.

        Parameters
        ----------
        image : str
            Filename for a FITS image to be corrected.
        method : str, optional
            Method of creating the map. One of:
                * 'mean' 
                * 'median'
                * 'linear_screen'
                * 'quadratic_screen'
                * 'nearest_neighbour'
                * 'linear_interpolation'
                * 'declination' 
                * 'elevation' 
                * 'RBF_linear'
        memfrac : float, optional
            Fraction of available RAM to use. [Default 0.5]
        absmem : various, optional
            Absolute memory to use. If 'all', then available memory times ``memfrac``
            will be used. [Default 'all']
        outname : str, optional
            Output file name for the correction factor map. Default is based on the
            image name and chosen method.
        smooth : int, optional
            For RBF functions that take a ``smooth`` parameter, this is passed
            to them. In pixels. [Default 0]

        """

        self.model = None
        self.get_statistics(verbose=True)

        if outname is None:
            outname = image.replace(".fits", "_{}_factors.fits".format(method))
        self.name = outname

        if "median" in method.lower():
            # Take the median value for the whole map:
            self.factors = np.full_like(np.squeeze(fits.getdata(image)), 
                                        self.median)
            self.method = "median"

        elif "mean" in method.lower():
            # Take the SNR-weight mean value (good only if sigma clipped!)
            self.factors = np.full_like(np.squeeze(fits.getdata(image)), 
                                        self.weighted_mean)
            self.method = "mean"

        elif "screen" in method.lower():
            if "quad" in method.lower():
                screen = Quadratic2D()
                self.method = "quadratic_screen"
                order = 2
            elif "lin" in method.lower():
                screen = Linear2D()
                self.method = "linear_screen"
                order = 1
            # elif "gauss" in method.lower():
            #     screen = Gaussian2D()
            #     self.method = "gaus_2d"
            #     order = 1
            else:
                # logger.warning("using Quadratic2D screen.")
                # screen = Quadratic2D()
                # screen = 
                # self.method = "quadratic_screen"
                # TODO make this work:
                screen = Poly2D(mtype="xy", order=order)

            self.factors = self.fit_screen(fitsimage=image,
                                           screen=screen)
            self.method = "screen{}".format(order)

        elif "gauss" in method.lower():
            screen = Gaussian2D()
            self.method = "gauss_2d"

            hdr = fits.getheader(image)
            
            screen.p0[2] = hdr["NAXIS1"] // 4
            screen.p0[3] = hdr["NAXIS2"] // 4
            screen.p0[0] = hdr["CRPIX1"]
            screen.p0[1] = hdr["CRPIX2"]

            self.factors = self.fit_screen(
                    fitsimage=image,
                    screen=screen,
                    nolog=True
                )
        

        elif "dec" in method.lower():
            # Fits a polynomial as a function of declination to the sources
            # and then apply to image pixels. A model object is returned that
            # can be used for result-plotting later on.

            self.factors, self.model = self.fit_coordinate(fitsimage=image,
                                                           order=order)
            self.method = "declination"

        elif "alt" in method.lower() or "elev" in method.lower():
            # As with declination but as a function of elevation:
            
            if "2d" in method.lower():

                self.factors, self.model = self.fit_coordinate_screen(
                    fitsimage=image,
                    time=time,
                    telescope=telescope,
                    screen=Quadratic2D())

            else:

                self.factors, self.model = self.fit_coordinate(fitsimage=image,
                                                               order=order,
                                                               time=time,
                                                               telescope=telescope)
            self.method = "elevation"

        else:

            if "rbf" in method.lower():
                # Linear RBF interpolation:
                interpolation = "linear"
                self.method = "linear_rbf"
            elif "linear" in method.lower():
                # Pure 2D linear interpolation:
                interpolation = "only_linear"
                self.method = "linear"
            elif "nearest" in method.lower():
                # Nearest neighbour interpolation:
                interpolation = "nearest"
                self.method = "nearest_neighbour"
            else:
                raise RuntimeError("{} not a supported method!".format(method))


            logger.info("passing {} off to sparse_grid_interpolator...".format(image))
            self.factors = rbf(image=image,
                               x=self.mdata.ra,
                               y=self.mdata.dec,
                               z=self.mdata.ratio,
                               interpolation=interpolation,
                               smooth=smooth,
                               world_coords=True,
                               memfrac=memfrac,
                               absmem=absmem,
                               outname=outname,
                               constrain=edge_interp,
                               return_arr=True)


    def get_statistics(self, verbose=True):
        """Get some simple statistics. 

        Additionally do some Gaussian fitting to this data.

        Parameters
        ----------
        verbose : bool, optional
            Select True if wanting printed output.

        """
    
        if self.offset:
            self.mean = np.mean(self.mdata.ratio)
            self.weighted_mean = np.average(self.mdata.ratio, 
                                            weights=self.mdata.snr)
        else:
            self.mean = 10**(np.mean(self.mdata.logratio))
            self.weighted_mean = 10**(np.average(self.mdata.logratio,
                                                 weights=self.mdata.snr))

        self.std = np.std(self.mdata.ratio)
        self.median = np.median(self.mdata.ratio)


        if self.tdata is not None:

            if self.offset:
                self.tmean = np.mean(self.tdata.ratio)
                self.tweighted_mean = np.average(self.tdata.ratio, 
                                                 weights=self.tdata.snr)
            else:
                self.tmean = 10**(np.mean(self.tdata.logratio))
                self.tweighted_mean = 10**(np.average(self.tdata.logratio,
                                                      weights=self.tdata.snr))

            self.tstd = np.std(self.tdata.ratio)
            self.tmedian = np.median(self.tdata.ratio)

        if verbose:

            if self.offset:
                nametype = "offset"
            else:
                nametype = "ratio"

            logger.info("mean {:6}       = {:.4f}".format(nametype, self.mean))
            logger.info("SNR-weighted mean = {:.4f}".format(self.weighted_mean))
            logger.info("median            = {:.4f}".format(self.median))
            logger.info("standard dev.     = {:.4f}".format(self.std))
            logger.info("min flux density  = {:.4f} Jy".format(min(self.mdata.pflux)))
            logger.info("max flux density  = {:.4f} Jy".format(max(self.mdata.pflux)))

            if self.tdata is not None:

                logger.info("test mean {:6}       = {:.4f}".format(nametype, self.tmean))
                logger.info("test SNR-weighted mean = {:.4f}".format(self.tweighted_mean))
                logger.info("test median            = {:.4f}".format(self.tmedian))
                logger.info("test standard dev.     = {:.4f}".format(self.tstd))
                logger.info("min test flux density  = {:.4f} Jy".format(min(self.tdata.pflux)))
                logger.info("max test flux density  = {:.4f} Jy".format(max(self.tdata.pflux)))


    def get_residuals(self, header):
        """Get residuals.

        Defined as log(S_m/S_p) - log(correction_factor) or
        (S_m - S_p) - correction_offset.

        Parameters
        ----------
        header : astropy.io.fits.Header
            Header of reference FITS image for the CF map.  

        """

        wcs = WCS(header).celestial
        y, x = wcs.all_world2pix(self.mdata.ra, self.mdata.dec, 0)
        x = x.astype("i")
        y = y.astype("i")
        self.mdata.model = self.factors[x, y].flatten()
        if self.offset:
            # offset should not be log-ified:
            self.mdata.logmodel = self.mdata.model
        else:
            self.mdata.logmodel = np.log10(self.mdata.model)
            
        self.mdata.logresidual = self.mdata.logratio - self.mdata.logmodel
        # self.mdata.residual = self.mdata.ratio - self.mdata.model
        self.mdata.residual = self.mdata.ratio / self.mdata.model

        if self.tdata is not None:
            y, x = wcs.all_world2pix(self.tdata.ra, self.tdata.dec, 0)
            x = x.astype("i")
            y = y.astype("i")
            self.tdata.model = self.factors[x, y].flatten()
            if self.offset:
                self.tdata.logmodel = self.tdata.model
            else:
                self.tdata.logmodel = np.log10(self.tdata.model)

            self.tdata.logresidual = self.tdata.logratio - self.tdata.logmodel
            self.tdata.residual = self.tdata.ratio / self.tdata.model
                

    def check_within_image(self, image):
        """Check that calibrators have pixel coordinates within the image.

        Parameters
        ----------
        image : str
            FITS image.

        """

        hdu = fits.open(image)[0]
        img = np.squeeze(hdu.data)
        hdr = hdu.header
        wcs = WCS(hdr).celestial

        y, x = wcs.all_world2pix(self.mdata.ra, self.mdata.dec, 0)
        x = x.astype("i")
        y = y.astype("i")
        outside = []
        for i in range(len(x)):
            x_i, y_i = x[i], y[i]  # Outside try block specifically
            try:
                img[x_i, y_i]
            except IndexError:
                # Calibrator has coordinates outside of the image:
                outside.append(i)
        mask = np.zeros(len(self.mdata), bool)
        mask[outside] = 1
        self.mdata = self.mdata[~mask]

        if self.tdata is not None:
            y, x = wcs.all_world2pix(self.tdata.ra, self.tdata.dec, 0)
            x = x.astype("i")
            y = y.astype("i")
            outside = []
            for i in range(len(x)):
                x_i, y_i = x[i], y[i]
                try:
                    img[x_i, y_i]
                except IndexError:
                    outside.append(i)
            mask = np.zeros(len(self.tdata), bool)
            mask[outside] = 1
            self.tdata = self.tdata[~mask]



    def get_minmax(self, name):
        """Convenience method to get min and max values from mdata/tdata.

        Parameters
        ----------
        name : str
            Field name in the mdata/tdata numpy.recarrays.

        """
   
        if self.tdata is None:
            vmin = min(self.mdata[name])
            vmax = max(self.mdata[name])
        else:
            vmin = min([min(self.mdata[name]), min(self.tdata[name])])
            vmax = max([max(self.mdata[name]), max(self.tdata[name])])

        return vmin, vmax


    def writeout_results(self, outname):
        """Writeout the results as applied to the calibrator sources.

        Parameters
        ----------
        outname : str
            Output filename. Does NOT determine the filetype.

        """

        with open(outname, "w+") as f:
            f.write("#ra dec flux model_flux ratio model_ratio residual snr\n")
            for i in range(len(self.mdata.ratio)):
                f.write("{0: 6.3f} {1: 6.3f} {6: 8.5f} {7: 8.5f} {2: 6.3f} {3: 6.3f} {4: 6.3f} {5: 6.1f}\n".format(
                    self.mdata.ra[i],
                    self.mdata.dec[i],
                    self.mdata.ratio[i],
                    self.mdata.model[i],
                    self.mdata.residual[i],
                    self.mdata.snr[i],
                    self.mdata.mflux[i],
                    self.mdata.pflux[i]))




def check_keys(table, *keys):
    """
    """

    for key in keys:
        if key not in table.names:
            raise KeyError("{} not in model catalogue!".format(key))


def writeout_region(ra, dec, ratio, outname, color="green"):
    """Write out a DS9 region file of the calibrators."""

    with open(outname, "w+") as f:
        for i in range(len(ra)):
            size = ratio[i]  # based on ratio?
            f.write("fk5;circle {}d {}d {}d # color={}\n".format(ra[i], 
                                                                 dec[i], 
                                                                 size, 
                                                                 color))


def load_data(filename, delimiter=""):
    """
    """

    data = np.genfromtxt(filename, delimiter=delimiter)

    if data.shape[0] == 3:
        return data[:, 0], data[:, 1], data[:, 2], None
    else:
        return data

def get_snr(indices, table, flux_key="flux", localrms_key="local_rms"):
    """
    """

    if localrms_key in table.names:
        snr = table[flux_key][indices] / table[localrms_key][indices]
    else:
        snr = np.full_like(indices, 1.)

    return snr


def sigma_clip(ratios, indices, sigma=2., table=None, outname=None):
    """Sigma-clip the ratios - remove 'outliers'.

    This might seem harsh but ultimately our cross-matching is the likeliest 
    cause of large deviations. We do not expect our field to have such a high
    variation in flux density ratios, and if so they are not correctable.

    Optionally write out the outliers for QA.
    
    TODO: offer warning if large fraction clipped?
    """

    avg = np.nanmean(ratios)
    logger.debug("avg: {}".format(avg))
    std = np.nanstd(ratios)
    logger.debug("std: {}".format(std))

    new_indices = np.where(abs(ratios-avg) < sigma*std)[0]

    if table is not None and outname is not None:
       
        clipped_indices = np.where(abs(ratios-avg) >= sigma*std)[0]
        clipped_ratios = ratios[clipped_indices]
        clipped_ra = table["ra"][clipped_indices]
        clipped_dec = table["dec"][clipped_indices]

        writeout_region(ra=clipped_ra,
                        dec=clipped_dec,
                        ratio=clipped_ratios,
                        outname=outname,
                        color="yellow")

    return ratios[new_indices], indices[new_indices]


def nsrc_cut(table, flux_key, indices, nsrc_max, ratios, offset=False):
    """Change threshold to ensure ``< nsrc_max`` sources are used.

    Threshold is based on 'corrected flux' rather than measured.
    """

    if offset:
        cflux = table[indices][flux_key] - ratios
    else:
        cflux = table[indices][flux_key] / ratios

    if len(cflux) > nsrc_max:
        threshold = round(np.sort(cflux)[::-1][nsrc_max-1], 1)
        indices = indices[cflux > threshold]
        ratios = ratios[cflux > threshold]
        logger.info("New threshold set to {:.1f} Jy".format(threshold))

    return ratios, indices



def ensure_within_array(fitsimage, ra, dec, ratios):
    """
    """

    with fits.open(fitsimage) as f:
        w = WCS(f[0].header).celestial
        y, x = w.all_world2pix(ra, dec, 0)
        x = x.astype("i")
        y = y.astype("i")

        arr = np.squeeze(f[0].data)
        for i in range(len(ratios)):
            try:
                _ = f[0].data[x[i], y[i]]
            except IndexError:
                ratios[i] = np.nan
        valid = ~np.isnan(ratios)

    return ra[valid], dec[valid], ratios[valid]



def weighted_mean(ratios, snr):
    """Simple wrapper for calculating the weighted mean of an array."""
    return np.average(ratios, weights=snr)


def fit_coordinate(dec, ratios, fitsimage, outname, ra=None, snr=None, order=2,
                   stride=225000, time=None, telescope="MWA"):
    """"
    Fit a polynomial to the calibrator sources as a function of declination
    or elevation. 

    Parameters
    ----------
    dec : array-like
        1-D array of DEC of calibrator sources.
    ratios : array-like
        1-D array of values to fit corresponding to ``dec``.
    fitsimage : str
        Filename for a FITS image. Required for converting from WCS and pixels.
    outname : str
        Output filename for the fitted map.
    ra : array-like, optional
        1-D array of RA of calibrator sources. Required for elevation fitting. 
        [Default None]
    snr : array-like or None
        1-D array of SNR values corresponding to ``ra`` or None. This provides a 
        weight for fitting the screen.
    order : int, optional
        Order of the polynomial, with 0 == constant. [Default 2]
    stride : int, optional
        Number of pixels to evaluate at a time. Conversion between pixel and 
        world coordinates can be memory intensive for large arrays, so we do it
        in chunks. [Default 225000]
    time : `~astropy.time.Time` object, optional
        An `~astropy.time.Time` object of the observation. Required for elevation
        fitting. [Default None]
    telescope : str, optional
        Telescope name from `flux_warp.models.TELESCOPES`. Required for elevation
        fitting. [Default 'MWA'].

    Returns
    -------
    flux_warp.models.Poly1D object
        Optimized parameters are stored and returned as part of this object.


    A FITS image of the correction factors evaluated from the dec- or alt- 
    fitting is written to disk.

    """

    if snr is None:
        snr = np.full_like(ratios, 1.)

    ref = fits.open(fitsimage)

    if time is not None and ra is not None:
        logger.info("fitting 1-D polynomial as a function of elevation")
        model = Poly1D(mtype="elevation", order=order)

        coords_radec = FK5(ra=np.asarray(ra)*u.deg, dec=np.asarray(dec)*u.deg)
        coords_altaz = coords_radec.transform_to(AltAz(obstime=time,
            location=TELESCOPES[telescope]))

        coordinate = coords_altaz.alt.value


    else:
        logger.info("fitting 1-D polynomial as a function of declination")
        model = Poly1D(mtype="declination", order=order)

        coordinate = dec


    # np.float32 dtype required? - failed to fit otherwise
    popt, pcov = curve_fit(model.evaluate,
                           xdata=np.asarray(coordinate).astype(np.float32),
                           ydata=np.log10(np.asarray(ratios).astype(np.float32)),
                           # sigma=1./np.asarray(snr).astype(np.float32),
                           sigma=1./(np.asarray(snr)**2).astype(np.float32),
                           absolute_sigma=True,
                           p0=model.p0,
                           method="lm")
    perr = np.sqrt(np.diag(pcov))

    model.store_params(popt)

    w = WCS(ref[0].header).celestial
    correction_arr = np.full_like(np.squeeze(ref[0].data), 1.)
    indices = np.indices(correction_arr.shape)
    x = indices[0].flatten()
    y = indices[1].flatten()
    n = len(x)

    for i in range(0, n, stride):

        sys.stdout.write(u"\u001b[1000D" + "{:.>6.1f}%".format(100.*i/n))
        sys.stdout.flush()

        r, d = w.all_pix2world(y[i:i+stride], x[i:i+stride], 0)
        if model.mtype == "elevation":
            # coordinate transformation takes time, hence making the 
            # strides
            radec_i = FK5(ra=r*u.deg, dec=d*u.deg)
            altaz_i = radec_i.transform_to(AltAz(obstime=time, 
                                                 location=TELESCOPES[telescope]))
            coordinate = altaz_i.alt.value
        else:
            coordinate = d

        correction_arr[x[i:i+stride], y[i:i+stride]] = 10**Poly1D.evaluate(coordinate, 
                                                                           *popt)

    print("")

    return correction_arr, model


def fit_screen(ra, dec, ratios, fitsimage, outname, snr=None, screen=Quadratic2D):
    """Fit a screen to ratios to get a correction factor map.

    Parameters
    ----------
    ra : array-like
        1-D array of RA.
    dec : array-like
        1-D array of DEC corresponding to ``ra``.
    ratios : array-like
        1-D array of values to fit corresponding to ``ra``.
    fitsimage : str
        Filename for a FITS image. Required for converting from WCS and pixels.
    outname : str
        Output filename for the fitted map.
    snr : array-like or None
        1-D array of SNR values corresponding to ``ra`` or None. This provides a 
        weight for fitting the screen.
    screen : `~flux_warp.models.Linear2D` or `~flux_warp.models.Quadtratic2D`
        Screen to fit provided in `~flux_warp.models`. At the moment, one of:
            * `~flux_warp.models.Linear2D`
            * `~flux_warp.models.Quadtratic2D`

    """

    if snr is None:
        snr = np.full_like(ratios, 1.)


    with fits.open(fitsimage) as ref:
    
        ra = np.asarray(ra)
        dec = np.asarray(dec)
        ratios = np.asarray(ratios).astype(np.float32)

        w = WCS(ref[0].header).celestial

        y, x = w.all_world2pix(ra, dec, 0)
        x = x.astype("i")
        y = y.astype("i")

        f = np.full_like(np.squeeze(ref[0].data), np.nan)
        indices = np.indices(f.shape)
        xi = indices[0].flatten()
        yi = indices[1].flatten()
        # r, d = w.all_pix2world(yi, xi, 0)

        params = screen.p0
        popt, pcov = curve_fit(screen.evaluate,
                               xdata=np.asarray([x, y]),
                               # xdata=np.asarray([ra, dec]), 
                               ydata=np.log10(ratios),
                               # sigma=1./np.asarray(snr).astype(np.float32),
                               sigma=np.asarray(snr).astype(np.float32),
                               absolute_sigma=True,
                               p0=params,
                               method="trf")

        # indices = np.indices(f.shape)

        # xi = indices[0].flatten()
        # yi = indices[1].flatten()

        f[xi, yi] = 10**screen.evaluate((xi, yi), *popt)

        # fits.writeto(outname, f, ref[0].header, overwrite=True)

    return f


def fluxscale(table, freq, threshold=1., nsrc_max=100, flux_key="flux",
              ra_key="ra", dec_key="dec", exclude_large_sources=True, 
              powerlaw_keys=["alpha_p", "beta_p"], 
              powerlaw_index_limits=(-2., 0.5),
              cpowerlaw_keys=["alpha_c", "beta_c", "gamma_c"], curved=True,
              ref_flux_key="S154", ref_freq=154., extrapolate=True, 
              spectral_index=-0.77, localrms_key=None,  nmeas_key="nmeas",
              nmeas=1., offset=False,
              powerlaw_from_ref=False):
    """Calculate the ratio S_measured/S_predicted for sources in a table.

    This uses the model parameters for powerlaw (and optionally curved powerlaw)
    fits to determine expected flux density values. Optionally flux densities
    can be extrapolated using a provided spectral index (hence assuming a 
    simple powerlaw model if a flux density measurement is available).

    Parameters
    ----------
    table : str
        Filename of the model FITS table. 
    freq : float
        Frequency in MHz at which to predict flux densities.
    threshold : float, optional
        Sources below threshold (predicted) are ignored. [Default 1 Jy]
    nsrc_max : int, optional
        Max number of sources to include. The threshold is adjusted to make
        sure the number of sources is less than this, though note that this
        might result in there being 1-10 sources fewer than nsrc_max.
        [Default 100]
    flux_key : str, optional
        Column name in FITS table for measured flux densities. [Default 'flux']
    ra_key : str, optional
        Column name in FITS table for true RA. [Default 'ra']
    dec_key : str, optional
        Column name in FITS table for true DEC. [Default 'dec']
    exclude_large_sources : bool, optional
        Exclude sources within/near large sources (LMC and SMC). [Default True]
    powerlaw_keys : list, optional
        A 2-length list of keys that describe the powerlaw model parameters.
        [Default ['alpha_p', 'beta_p']]
    powerlaw_index_limits : tuple, optional
        Limits for powerlaw index. [Default (-2., 0.5)]
    cpowerlaw_keys ; list, optional
        A 3-length list of keys that descibe the curved powerlaw model parameters.
        [Default ['alpha_c', 'beta_c', 'gamma_c']]
    curved : bool, optional
        Select if wanting to using curved power law models. This is switched
        off if the cpowerlaw_keys are not present in the table, or if values
        are NaN. [Default True]
    ref_flux_key : str, optional
        Column name in FITS table for reference flux density value. [Default 'S154']
    ref_flux_freq : float, optional
        Reference frequency in MHz corresponding to reference flux density. 
        [Default 154.]
    extrapolate : bool, optional
        Select if wanting to extrapolate flux densities from the reference
        flux densitiy, in the absence of powerlaw or curved powerlaw 
        models. [Default True]
    spectral_index : float, optional
        The powerlaw index to extrapolate flux density from if extrapolate is 
        selected. [Default -0.77]
    localrms_key : str, optional
        Column name in FITS table for local rms values. Used to calculate the SNR
        for SNR-weighting methods. [Default 'local_rms']
    nmeas_key : str, optional
        Column name in FITS table that notes the number of measurements a source
        has. [Default 'nmeas']
    nmeas : int, optional
        If nmeas_key is specified, then this is the the number of measurements
        that are required before a source is to be considered. [Default 1]


    Returns
    -------
    p_ratios : `~np.ndarray`
        1-D array of ratios (i.e. correction factors).
    p_indices : `~np.ndarray`
        1-D array of indices of the ratios that go into the input table.
    all_ratios : `~np.ndarray`
        1-D array of ALL indices prior to clipping.
    snr : `~np.ndarray`
        1-D array of SNR of measured flux densities if a ``localrms_key`` is
        supplied and present in the table. 


    
    Note that many of the defaults work well when using the table file
    'pumav3_full_spectral_params.fits' cross-matched to an Aegean output
    .fits or .vot file using `~match_catalogues`. 
    """


    # Open table if needed:
    if isinstance(table, str):
        table = fits.open(table)[1].data

    _powerlaw = True

    # Verify that certain keys are in the table:
    if not (nmeas_key in table.names):
        nmeas_key = None
        logger.warning("No '{}' in table, using any number of "
                       "measurements.".format(nmeas_key))
    if not np.asarray([key in table.names for key in powerlaw_keys]).all():
        _powerlaw = False
        logger.warning("No '{}' in table, not using powerlaw model.".format(
                       powerlaw_keys))
    if not np.asarray([key in table.names for key in cpowerlaw_keys]).all():
        curved = False
        logger.warning("No '{}' in table, not using curved powerlaw "
                       "model.".format(cpowerlaw_keys))
    if not (ref_flux_key in table.names):
        extrapolate = False
        logger.warning("No '{}' in table, not using extrapolation.".format(
                       ref_flux_key))
    if localrms_key is not None:
        if not localrms_key in table.names:
            localrms_key = None
            logger.warning("No '{}' in table, not using local rms for SNR "
                           "weighting.".format(localrms_key))

    if not extrapolate and not powerlaw and not curved:
        raise RuntimeError("No way to calculate model fluxes! \n"
                           "Either powerlaw or curved powerlaw model parameters "
                           "should be specified in the model table, "
                           "or you must allow for extrapolation and have a "
                           "reference flux density value to extrapolate from.")

    if powerlaw_from_ref and (ref_flux_key is None \
                              or ref_freq is None \
                              or cpowerlaw_keys is None):
        raise RuntimeError("Using the curved powerlaw from a reference "
                           "measurement requires ref_flux_key, ref_freq, and "
                           "cpowerlaw_keys (with the first key ignored).")


    predicted_flux, indices, ratios = [], [], []

    for i in range(len(table)):

        # Check if the required number of measurements were made:
        if nmeas_key is not None:
            if table[nmeas_key][i] < nmeas:
                logger.debug("skipping {} as it has {} measurements".format(i, table[nmeas_key][i]))
                continue

        # Exclude sources within a certain radius around certain large sources
        # like the LMC or SMC where we do not have good models. 
        if exclude_large_sources:

            in_excl = False

            source_coords = SkyCoord(ra=table[ra_key][i], 
                                     dec=table[dec_key][i],
                                     unit=(u.deg, u.deg))

            for excl in SOURCES:
                sep_excl = excl.coords.separation(source_coords)
                if sep_excl.value <= excl.radius:
                    logger.debug("skipping {} as it is within the {}".format(i, excl.name))
                    in_excl = True

            if in_excl:
                continue

        f = np.nan

        # First try a curved model:
        if curved:
            if not np.isnan(table[cpowerlaw_keys[-1]][i]):


                if powerlaw_from_ref:
                    f = cpowerlaw_from_ref(freq, ref_freq, table[ref_flux_key][i],
                                           *[table[p][i] for p in cpowerlaw_keys[1:]])
                else:
                    f = cpowerlaw(freq, *[table[p][i] for p in cpowerlaw_keys])


        # Second try a generic powerlaw model:
        if _powerlaw and np.isnan(f):
            if not np.isnan(table[powerlaw_keys[0]][i]):
                # Perform a check here to ignore poor powerlaw fits?
                if (powerlaw_index_limits[0] <= table[powerlaw_keys[1]][i] 
                    <= powerlaw_index_limits[1]):

                    f = powerlaw(freq, *[table[p][i] for p in powerlaw_keys])

        # Lastly try to extrapolate:
        if extrapolate and np.isnan(f):

            f = from_index(freq, ref_freq, table[ref_flux_key][i], spectral_index)

        # Quit now if we couldn't get a flux density prediction:
        if np.isnan(f):
            continue

        if f > threshold:

            indices.append(i)
            predicted_flux.append(f)

            if offset:
                ratios.append(table[flux_key][i] - f)
            else:
                ratios.append(table[flux_key][i]/f)

            if localrms_key is not None:
                # We will weight by the SNR later on if fitting/taking the mean.
                table[localrms_key][i] = (table[flux_key][i] / 
                                          table[localrms_key][i])


    valid = np.isfinite(ratios)
    ratios = np.asarray(ratios)[valid]
    indices = np.asarray(indices)[valid]


    logger.info("number of calibrators prior to clipping: {}".format(len(indices)))

    all_ratios = [ratios]

    p_ratios, p_indices = sigma_clip(ratios=np.asarray(ratios), 
                                     indices=np.asarray(indices),
                                     table=table,
                                     sigma=3)
    p_ratios, p_indices = sigma_clip(ratios=p_ratios,
                                     indices=p_indices,
                                     table=table,
                                     sigma=2)

    
    logger.info("number of calibrators after clipping: {}".format(len(p_indices)))

    all_ratios.append(p_ratios)

    p_ratios, p_indices = nsrc_cut(table, flux_key, p_indices, 
                                 nsrc_max, p_ratios)

    all_ratios.append(p_ratios)

    logger.info("number of calibrators: {}".format(len(p_indices)))

    if localrms_key is not None:
        snr = table[localrms_key][p_indices]
    else:
        snr = np.full_like(p_indices, 1.)

    return p_ratios, p_indices, all_ratios, snr, 



def get_test_sources(table, ratios, indices, faint=False, 
                     flux_key="flux", test_fraction=0.1):
    """Get a subset of sources for testing e.g. residuals.
    """

    test_ratios, test_indices = [], []
    cal_ratios, cal_indices = [], []

    if faint:
        threshold = np.percentile(table[flux_key][indices], test_fraction*100., 
                                  interpolation="lower")
        logger.info("setting new threshold to {:.2f} Jy for calibrators".format(threshold))
        

        cal_ratios = np.asarray([ratios[i] for i in range(len(ratios)) if 
            table[flux_key][indices[i]] > threshold])
        cal_indices = np.asarray([indices[i] for i in range(len(ratios)) if 
            table[flux_key][indices[i]] > threshold])
        test_ratios = np.asarray([ratios[i] for i in range(len(ratios)) if 
            table[flux_key][indices[i]] <= threshold]) 
        test_indices = np.asarray([indices[i] for i in range(len(ratios)) if 
            table[flux_key][indices[i]] <= threshold])

    else:

        # take every N sources ordered by brightness:

        flux = table[flux_key][indices]
        m = np.asarray([flux, indices, ratios]).T
        m = m[m[:, 0].argsort()]  # from faint to bright
        n = 1./test_fraction
        for i in range(len(indices)):
            if i%n == 0:
                test_ratios.append(ratios[i])
                test_indices.append(indices[i])
            else:
                cal_ratios.append(ratios[i])
                cal_indices.append(indices[i])


    logger.info("final final number of calibrators: {}".format(len(cal_indices)))

    return cal_ratios, cal_indices, test_ratios, test_indices


def get_corresponding_offset(offset_map, offset_wcs, ra, dec):
    """
    """

    y, x = offset_wcs.all_world2pix(ra, dec, 0)
    x = x.astype("i")
    y = y.astype("i")
    offset = offset_map[x, y]

    return offset


def flux_ratios(table, freq, threshold=0., nsrc_max=100,
                # Table keys: 
                flux_key="flux",
                ra_key="ra",
                dec_key="dec",
                powerlaw_amplitude=None,
                powerlaw_index=None,
                powerlaw_curvature=None,
                flux0=None,
                freq0=None,
                alpha0=None,
                localrms=None,
                nmeas=1.,
                nmeas_key="nmeas",
                offset=False,
                offset_image=None,
                exclude_large_sources=True,
                test_fraction=0.1,
                ):
    """

    model0 = curved powerlaw
    model1 = curved powerlaw from reference
    model2 = powerlaw
    model3 = powerlaw from reference
    model4 = powerlaw from reference + alpha
    """

    # Open table if needed:
    if isinstance(table, str):
        tabe = fits.open(table)[1].data

    if offset_image is not None:
        offset_hdu = fits.open(offset_image)[0]
        offset_wcs = WCS(offset_hdu.header).celestial

    model0 = model1 = model2 = model3 = model4 = False

    if None not in [powerlaw_amplitude, powerlaw_curvature, powerlaw_index]:
        check_keys(table, powerlaw_amplitude, powerlaw_curvature, powerlaw_index)
        logger.info("evaluating models using curved powerlaw model.")
        model0 = True
    if None not in [powerlaw_curvature, powerlaw_index, flux0, freq0]:
        check_keys(table, powerlaw_curvature, powerlaw_index, flux0)
        logger.info("evaluating models using curved powerlaw model from a "
                    "reference frequency.")
        model1 = True
    if None not in [powerlaw_amplitude, powerlaw_index]:
        check_keys(table, powerlaw_amplitude, powerlaw_index)
        logger.info("evaluating models using a generic power law model.")
        model2 = True
    if None not in [powerlaw_index, flux0, freq0]:
        check_keys(table, powerlaw_index, flux0)
        logger.info("evaluating models using a generic power law model from a "
                    "reference frequency.")
        model3 = True
    if None not in [flux0, freq0, alpha0]:
        check_keys(table, flux0)
        logger.info("evaluating models using a generic power law from a "
                    "reference frequency and assumed spectral index.")
        model4 = True
    if not (model0 or model1 or model2 or model3 or model4):
        raise RuntimeError("not enough information to evaluate models!")

    if not (nmeas_key in table.names):
        nmeas_key = None
        logger.warning("using any number of measurements.")

    if not (localrms in table.names):
        localrms = None
        logger.warning("no local rms column in model catalogue.")


    fm = FluxMatch()


    table.sort(axis=0, order=flux_key)  # easier to deal with later on

    predicted_flux, indices, ratios = [], [], []

    for i in range(len(table)):

        # Check if the required number of measurements were made:
        if nmeas_key is not None:
            if table[nmeas_key][i] < nmeas:
                logger.debug("skipping {} as it has {} measurements".format(i, table[nmeas_key][i]))
                continue

        # Exclude sources within a certain radius around certain large sources
        # like the LMC or SMC where we do not have good models. 
        if exclude_large_sources:

            in_excl = False

            source_coords = SkyCoord(ra=table[ra_key][i], 
                                     dec=table[dec_key][i],
                                     unit=(u.deg, u.deg))

            for excl in SOURCES:
                sep_excl = excl.coords.separation(source_coords)
                if sep_excl.value <= excl.radius:
                    logger.debug("skipping {} as it is within the {}".format(i, excl.name))
                    in_excl = True

            if in_excl:
                continue


        f = np.nan


        if model0:
            # Curved power law from full model components:
            if not np.asarray([np.isnan(table[key][i]) for key in 
                [powerlaw_index, powerlaw_amplitude, powerlaw_curvature]]).all():

                f = cpowerlaw(freq, 
                              a=table[powerlaw_amplitude][i], 
                              b=table[powerlaw_index][i], 
                              c=table[powerlaw_curvature][i])

        if model1 and np.isnan(f):
            # Curved power law from reference measurement:
            if not np.asarray([np.isnan(table[key][i]) for key in
                [powerlaw_index, powerlaw_curvature, flux0]]).all():

                a = cpowerlaw_amplitude(x0=freq0,
                                        y0=table[flux0][i],
                                        b=table[powerlaw_index][i],
                                        c=table[powerlaw_curvature][i])
                f = cpowerlaw(freq, 
                              a=a, 
                              b=table[powerlaw_index][i], 
                              c=table[powerlaw_curvature][i])

        if model2 and np.isnan(f):
            # Normal power law from full model components:
            if not np.asarray([np.isnan(table[key][i]) for key in
                [powerlaw_amplitude, powerlaw_index]]).all():

                f = cpowerlaw(freq,
                              a=table[powerlaw_amplitude][i],
                              b=table[powerlaw_index][i],
                              c=0.)

        if model3 and np.isnan(f):
            # Normal power law from reference measurement:
            if not np.asarray([np.isnan(table[key][i]) for key in
                [powerlaw_index, flux0]]).all():

                a = cpowerlaw_amplitude(x0=freq0,
                                        y0=table[flux0][i],
                                        b=table[powerlaw_index][i],
                                        c=0)
                f = cpowerlaw(freq,
                              a=a,
                              b=table[powerlaw_index][i],
                              c=0.)

        if model4 and np.isnan(f):
            if not np.isnan(table[flux0][i]):

                f = from_index(freq, 
                               x1=freq0,
                               y1=table[flux0][i],
                               index=alpha0)

        if np.isnan(f):
            continue

        if f > threshold:

            f_measured = table[flux_key][i]

            indices.append(i)
            predicted_flux.append(f)
            
        
            if offset:
                ratios.append(table[flux_key][i] - f)
                

            else:
                if offset_image is not None:
                    offset_i = get_corresponding_offset(offset_hdu.data,
                                                        offset_wcs,
                                                        table[ra_key][i],
                                                        table[dec_key][i])
                    table[flux_key][i] -= offset_i
                ratios.append(table[flux_key][i]/f)


            if offset:
                r_i = f_measured - f
            else:
                r_i = f_measured / f

            if np.all(np.isfinite(np.array([r_i, f, f_measured]))):
                if localrms is not None:
                    snr_i = f_measured / table[localrms][i]
                else:
                    snr_i = 1.
                row_i = np.array([(i, table[ra_key][i], table[dec_key][i],
                                   f, f_measured, snr_i, r_i)], 
                                dtype=fm.dtypes)
                fm.mdata = np.resize(fm.mdata, fm.mdata.shape[0]+1).view(np.recarray)
                fm.mdata[-1] = row_i



    # valid = np.isfinite(ratios)
    # ratios = np.asarray(ratios)[valid]
    # indices = np.asarray(indices)[valid]

    # logger.info("number of calibrators prior to clipping: {}".format(len(indices)))
    logger.info("number of calibrators prior to clipping: {}".format(len(fm.mdata)))
    # all_ratios = [ratios]

    fm.all_ratios.append(fm.mdata.ratio)

    for sigma in [3, 2, 2]:
        # fm.ratios, fm.indices = sigma_clip(ratios=fm.ratios,
        #                                    indices=fm.indices,
        #                                    table=table,
        #                                    # outname=region_file_name.replace(".reg", 
        #                                    #    "_sigma{}.reg".format(sigma)),
        #                                    sigma=sigma)
        fm.sigma_clip(sigma)


    logger.info("number of calibrators after sigma-clipping: {}".format(len(fm.mdata)))

    # all_ratios.append(ratios)
    # ratios, indices = nsrc_cut(table, flux_key, indices, nsrc_max, ratios)
    # all_ratios.append(ratios)

    fm.all_ratios.append(fm.mdata.ratio)
    fm.nsrc_cut(nmax=nsrc_max)
    fm.all_ratios.append(fm.mdata.ratio)

    logger.info("final number of calibrators: {}".format(len(fm.mdata)))


    # Print some stats:


    # return ratios, indices, all_ratios
    return fm


# def correction_factor_map(image, pra, pdec, ratios, method="linear_screen",
#                           memfrac=0.5, absmem="all", outname=None,
#                           smooth=0, writeout=True, snr=None, plot=False,
#                           time=None, telescope=None, order=2):
def correction_factor_map(image, fluxmatch, method="linear_screen",
                          memfrac=0.5, absmem="all", outname=None,
                          smooth=0, writeout=True, time=None, telescope=None,
                          order=2):
    """Create the correction factor map.

    Parameters
    ----------
    image : str 
        Filename for the FITS image to be corrected.
    pra : array-like
        Array or list of RA values for calibrator sources.
    pdec : array-like
        DEC corresponding to pra.
    ratios : array-like
        Ratios of S_measured/S_predicted corresponding to pra.
    method : str, optional
        Method of creating the map. One of:
            * 'mean' 
            * 'median'
            * 'linear_screen'
            * 'quadratic_screen'
            * 'nearest_neighbour'
            * 'linear_interpolation' 
            * 'RBF_linear'
    memfrac : float, optional
        Fraction of available RAM to use. [Default 0.5]
    absmem : various, optional
        Absolute memory to use. If 'all', then available memory times ``memfrac``
        will be used. [Default 'all']
    outname : str, optional
        Output file name for the correction factor map. Default is based on the
        image name and chosen method.
    smooth : int, optional
        For RBF functions that take a ``smooth`` parameter, this is passed
        to them. In pixels. [Default 0]
    writeout : bool, optional
        Select ``True`` if wanting to write out a region file showing the calibrator 
        sources. [Default True]
    snr : array-like, optional
        1-D array of SNR values corresponding to ``pra``. Used for weighted-mean
        and weighted-fitting of linear and quadratic screens. [Default None]

    Returns
    -------
    outname : str
        Output correction factor map name. The file is written to disk.


    """




    model = None

    if writeout:
        writeout_region(fluxmatch.mdata.ra, fluxmatch.mdata.dec, 
                        fluxmatch.mdata.ratio, 
                        image.replace(".fits", "_calibrators.reg"),
                        color="magenta")

    if outname is None:
        outname = image.replace(".fits", "_{}_factors.fits".format(method))

    if "median" in method.lower():
        # Take the median value for the whole map:
        factor = np.nanmedian(fluxmatch.mdata.ratio)
        # logger.info("median: {}".format(factor))
        with fits.open(image) as f:
            factors = np.full_like(np.squeeze(f[0].data), factor)
        method = "median"

    elif "mean" in method.lower():
        # Take the SNR-weight mean value (good only if sigma clipped!)

        factor = weighted_mean(fluxmatch.mdata.ratio, fluxmatch.mdata.snr)
        with fits.open(image) as f:
            factors = np.full_like(np.squeeze(f[0].data), factor)
        method = "mean"


    elif "screen" in method.lower():
        if "quad" in method.lower():
            screen = Quadratic2D()
            method = "quadratic_screen"
        elif "lin" in method.lower():
            screen = Linear2D()
            method = "linear_screen"
        else:
            logger.warning("using Quadratic2D screen.")
            screen = Quadratic2D()
            method = "quadratic_screen"

        factors = fit_screen(ra=fluxmatch.mdata.ra, 
                             dec=fluxmatch.mdata.dec, 
                             ratios=fluxmatch.mdata.ratio, 
                             fitsimage=image, 
                             outname=outname, 
                             snr=fluxmatch.mdata.snr, 
                             screen=screen)


    elif "dec" in method.lower():
        # Fits a polynomial as a function of declination to the sources
        # and then apply to image pixels. A model object is returned that
        # can be used for result-plotting later on.
        factors, model = fit_coordinate(dec=fluxmatch.mdata.dec, 
                                        ratios=fluxmatch.mdata.ratio, 
                                        image=image, 
                                        outname=outname, 
                                        snr=fluxmatch.mdata.snr, 
                                        order=order)
        method = "declination"

    elif "alt" in method.lower() or "elev" in method.lower():
        # As with declination but as a function of elevation:
        factors, model = fit_coordinate(dec=fluxmatch.mdata.dec, 
                                        ratio=fluxmatch.mdata.ratio, 
                                        image=image, 
                                        outname=outname, 
                                        snr=fluxmatch.mdata.snr, 
                                        order=order,
                                        ra=fluxmatch.mdata.ra,
                                        time=time,
                                        telescope=telescope)
        method = "elevation"

    else:

        if "rbf" in method.lower():
            # Linear RBF interpolation:
            interpolation = "linear"
            method = "linear_rbf"
        elif "linear" in method.lower():
            # Pure 2D linear interpolation:
            interpolation = "only_linear"
            method = "linear"
        elif "nearest" in method.lower():
            # Nearest neighbour interpolation:
            interpolation = "nearest"
            method = "nearest_neighbour"
        else:
            raise RuntimeError("{} not a supported method!".format(method))


        # sparse grid interpolation requires calibrators that are within the image?
        # pra, pdec, ratios = ensure_within_array(image, pra, pdec, ratios)


        logger.info("passing {} off to sparse_grid_interpolator...".format(image))
        factors = rbf(image=image,
                      x=fluxmatch.mdata.ra,
                      y=fluxmatch.mdata.dec,
                      z=fluxmatch.mdata.ratio,
                      interpolation=interpolation,
                      smooth=smooth,
                      world_coords=True,
                      memfrac=memfrac,
                      absmem=absmem,
                      outname=outname,
                      constrain=True,
                      return_arr=True
                      )

    fluxmatch.factors = factors
    fluxmatch.method = method
    fluxmatch.model = model
    fluxmatch.name = outname

    return fluxmatch


def apply_corrections(image, factors, freq, outname=None, factor_map_outname=None, 
                      overwrite=True, method="", offset=False,
                      no_write_factor_map=False):
    """Apply the correction factors.

    Parameters
    ----------
    image : str
        FITS image filename to apply correction factors to.
    correction_image : str
        FITS image filename containing correction factors.
    outname : str
        Output FITS filename.
    overwrite : bool, optional
        Select True if wanting to overwrite any existing files. [Default True]
    
    """

    if outname is None:
        outname = image.replace(".fits", "_{}.fits".format(method))
    if factor_map_outname is None:
        factor_map_outname = image.replace(".fits", "_CF{}".format(method))

    im = fits.open(image)

    logger.info("mean factor from map S_meas/S_predic   = {}".format(np.nanmean(factors)))
    logger.info("median factor from map S_meas/S_predic = {}".format(np.nanmedian(factors)))

    cdata = np.squeeze(im[0].data).copy()

    if offset:
        cdata -= factors
    else:
        cdata /= factors

    if "FREQ" not in im[0].header.keys():
        im[0].header["FREQ"] = freq*1.e6
    im[0].header["SCALEF"] = method
    
    fits.writeto(outname, cdata, im[0].header, overwrite=overwrite)
    if not no_write_factor_map:
        fits.writeto(factor_map_outname, factors, im[0].header, overwrite=overwrite)

    im.close()












