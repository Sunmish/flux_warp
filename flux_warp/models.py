import numpy as np
from astropy.coordinates import SkyCoord, EarthLocation
from astropy import units as u

# TODO all models as classes

class LargeSource():
    def __init__(self, name, ra, dec, radius):
        self.name = name
        self.coords = SkyCoord(ra=ra, dec=dec, unit=(u.hourangle, u.deg))
        self.radius = radius


class Quadratic2D():
    """Simple quadratic 2D screen."""
    def __init__(self):
        self.p0 = [1., 0., 0., 0., 0., 0.]
    @staticmethod
    def evaluate(xy, c0, c1, c2, c3, c4, c5):
        return (c0
                + c1*xy[0]
                + c2*xy[1] 
                + c3*np.power(xy[0], 2) 
                + c4*xy[0]*xy[1] 
                + c5*np.power(xy[1], 2)
                ) 
    @staticmethod
    def echo(*args):
        for arg in args:
            print("{}".format(arg)),
        print("")

class Poly2D():
    """Simple 2-d polynomial."""
    def __init__(self, mtype, order=1):
        self.p0 = [0.] + [0.]*((order+1)**2 - 1)
    @staticmethod
    def evaluate(xy, *c_ij):
        val = 0
        k = 0
        for i in range(int(np.sqrt(len(c_ij))-1)):
            for j in range(int(np.sqrt(len(c_ij))-1)):
                val += (c_ij[k]*(xy[0]**i)*(xy[1]**j))
                k += 1
            k += 1
        return val
    @staticmethod
    def echo(*args):
        for arg in args:
            print("{}".format(arg)),
        print("")


class Linear2D():
    """Simple linear 2D screen."""
    def __init__(self):
        self.p0 = [1., 0., 0.]
    @staticmethod
    def evaluate(xy, c0, c1, c2):
        return c0 + c1*xy[0] + c2*xy[1]
    @staticmethod
    def echo(*args):
        for arg in args:
            print("{}".format(arg)),
        print("")



class Poly1D():
    def __init__(self, mtype, order=1):
        self.p0 = [1.] + [0.]*(order)
        self.order = order
        self.mtype = mtype
    def store_params(self, params):
        self.params = params
    @staticmethod
    def evaluate(x, *args):
        return sum([args[i]*x**i for i in range(len(args))])


class Gaussian2D():
    def __init__(self):
        self.p0 = [0., 0., 1., 1., 0., 1.]
    @staticmethod
    def evaluate(xy, x0, y0, sigmax, sigmay, theta, A):
        return Gaussian2D.elliptical_gaussian_2d(xy[0], xy[1], x0, y0, sigmax, sigmay, theta, A)
    def _evaluate(self, xy):
        return Gaussian2D.elliptical_gaussian_2d(xy[0], xy[1], *self.p0)
    @staticmethod
    def echo(*args):
        for arg in args:
            print("{}".format(arg)),
        print("")
    @staticmethod
    def elliptical_gaussian_2d(x, y, x0, y0, sigma_x, sigma_y, theta=0, A=1):
        xp = (x-x0)*np.cos(theta) - (y-y0)*np.sin(theta)
        yp = (x-x0)*np.sin(theta) + (y-y0)*np.cos(theta)
        return A*np.exp(-(
            (xp**2 / (2*sigma_x**2)) + \
            (yp**2 / (2*sigma_y**2))
        ))

class Gaussian2D_1amp():
    def __init__(self):
        self.p0 = [0., 0., 1., 1., 0.]
    @staticmethod
    def evaluate(xy, x0, y0, sigmax, sigmay, theta):
        return Gaussian2D.elliptical_gaussian_2d(xy[0], xy[1], x0, y0, sigmax, sigmay, theta)
    def _evaluate(self, xy):
        return Gaussian2D.elliptical_gaussian_2d(xy[0], xy[1], *self.p0)
    @staticmethod
    def echo(*args):
        for arg in args:
            print("{}".format(arg)),
        print("")
    @staticmethod
    def elliptical_gaussian_2d(x, y, x0, y0, sigma_x, sigma_y, theta=0):
        xp = (x-x0)*np.cos(theta) - (y-y0)*np.sin(theta)
        yp = (x-x0)*np.sin(theta) + (y-y0)*np.cos(theta)
        return np.exp(-(
            (xp**2 / (2*sigma_x**2)) + \
            (yp**2 / (2*sigma_y**2))
        ))

def from_index(x, x1, y1, index):
    """Calculate flux from measured value and measured/assumed index."""
    return y1*(x/x1)**index

def two_point_index(x1, x2, y1, y2):
    """Calculate spectral index from two measurements."""
    return np.log10(y1/y2)/np.log10(x1/x2)

def powerlaw(x, a, b):
    """Simple powerlaw function."""
    return a*(x**b)

def cpowerlaw(x, a, b, c):
    """Simple curved powerlaw function."""
    return a*(x**b)*np.exp(c*np.log(x)**2)

def gaussian(x, a, x0, sigma):
    """Simple Gaussian function."""
    return a*np.exp(-(x-x0)**2 / (2.*sigma**2))

def cpowerlaw_from_ref(x, x0, y0, b, c):
    """Simple curved powerlaw function from reference value."""
    return y0*((x**b)/(x0**b)) * (np.exp(c*np.log(x)**2) / np.exp(c*np.log(x0)**2))

def cpowerlaw_amplitude(x0, y0, b, c):
    """Return amplitude of curved powerlaw model."""
    return y0 / (x0**b * np.exp(c*np.log(x0)**2))

def logx(x, a, b):
    """y=a*log(x+b)+c"""
    return a*np.log10(x)+b

class LogX():
    """
    """
    def __init__(self):
        self.p0 = [1., 0.]
    @staticmethod
    def evaluate(x, a, b):
        return a*np.log10(x)+b

def read_in_source(infile, sources):
    """Read in text file with name,hms,dms,radius for exclusion."""
    
    with open(infile) as f:
        lines = f.readlines()
        for line in lines:
            bits = line.split()
            sources.append(LargeSource(name=bits[0],
                                       ra=bits[1],
                                       dec=bits[2],
                                       radius=float(bits[3])))
    return sources

# Add more as you need:
SOURCES = [LargeSource("LMC", "05h23m35s", "-69d45m22s", 6.0),
           LargeSource("SMC", "00h52m38s", "-72d48m01s", 3.0),
           LargeSource("Virgo A", "12h30m49.4s", "+12d23m28s", 0.25),
           LargeSource("Fornax A", "03h22m41.7s", "-37d12m30s", 0.6),
           LargeSource("Pictor A", "05h19m49.7s", "-45d46m44s", 0.2)]

# Telescopes for elevation-dependent fluxscale corrections:
TELESCOPES = {"MWA": EarthLocation.from_geodetic(lat=-26.703319*u.deg, 
                                                 lon=116.67081*u.deg, 
                                                 height=377*u.m)
             }