from __future__ import print_function, division
from re import I 

import numpy as np
import os

from astropy.io import fits, votable
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.table import Table, hstack

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# Conversion between VOTable and FITS dtype:
# not needed when using astropy Table objects
DTYPE_TO_TFORM = {"f": "E",
                  "i": "I",
                  "S": "A",
                  "U": "A"}

POSSIBLE_RA_KEYS = ["RA", "ra", "_RAJ2000", "RAJ2000"]
POSSIBLE_DEC_KEYS = ["DEC", "Dec", "dec" ,"_DEJ2000", "DEJ2000", "DECJ2000"]
POSSIBLE_INT_FLUX_KEYS = ["int_flux", "Total_flux", "Total_flux_Source", "int_flux_wide", "Fintwide", "Fint", "S1_4", "Sint", "St", "Ftot", "Ftotc", "Stot"]
POSSIBLE_PEAK_FLUX_KEYS = ["peak_flux", "Peak_flux", "peak_flux_wide", "Fpwide", "Fp", "Sp", "Speak", "Fpeak", "Fpeakc"]



class Catalogue(object):
    """Easy handling of a catalogue."""


    def __init__(self, catalogue, 
                 ra_key=None, 
                 dec_key=None, 
                 flux_key=None, 
                 eflux_key=None,
                 peak_flux_key=None, 
                 localrms_key=None,
                 a_key=None, 
                 b_key=None, 
                 psf_a_key=None, 
                 psf_b_key=None,
                 pa_key=None,
                 flux_multiplier=1.):
        """When initialised, a number of items will be stored for later.

        Parameters
        ----------
        catalogue : str
            Filepath to a catalogue in a .fits or .vot format.
        ra_key : str
            Key for RA column in ``catalogue``.
        dec_key : str
            Key for DEC column in ``catalogue``.
        flux_key : str, optional
            Key for flux density. Not used if not supplied. [Default None]
        eflux_key : str, optional
            Key for error on flux density. [Default None]
        localrms_key : str, optional
            Key for local rms measurement. [Default None]


        """

        self.name = os.path.basename(catalogue)

        self.table = Catalogue.open_catalogue(catalogue)

        if ra_key is None:
            for key in POSSIBLE_RA_KEYS:
                if key in self.table.columns:
                    logger.debug("{} found for {}".format(key, self.name))
                    ra_key = key
        if dec_key is None:
            for key in POSSIBLE_DEC_KEYS:
                if key in self.table.columns:
                    logger.debug("{} found for {}".format(key, self.name))
                    dec_key = key

        if ra_key is None or dec_key is None:
            raise RuntimeError("No RA and/or DEC columns for {}".format(self.name))

        self.ra_key = ra_key
        self.dec_key = dec_key

        self.coords = SkyCoord(ra=self.table[self.ra_key],
                               dec=self.table[self.dec_key],
                               unit=(u.deg, u.deg)) 

        self.flux_key = None
        self.eflux_key = None
        self.peak_flux_key = None
        self.localrms_key = None
        self.a_key = None
        self.b_key = None
        self.psf_a_key = None
        self.psf_b_key = None
        self.pa_key = None
        self.flux_multiplier = flux_multiplier


        self.names = self.table.columns.copy()

        # Add key names for use later on, only if they exist
        # Check for some often-used keys
        if flux_key is None:
            for key in POSSIBLE_INT_FLUX_KEYS:
                if key in self.names:
                    logger.debug("{} found for {}".format(key, self.name))
                    flux_key = key
        if peak_flux_key is None:
            for key in POSSIBLE_PEAK_FLUX_KEYS:
                if key in self.names:
                    logger.debug("{} found for {}".format(key, self.name))
                    peak_flux_key = key

        if flux_key in self.names:
            self.flux_key = flux_key
        if eflux_key in self.names:
            self.eflux_key = eflux_key
        if peak_flux_key in self.names:
            self.peak_flux_key = peak_flux_key
        if localrms_key in self.names:
            self.localrms_key = localrms_key
        if a_key in self.names:
            self.a_key = a_key
        if b_key in self.names:
            self.b_key = b_key
        if psf_a_key in self.names:
            self.psf_a_key = psf_a_key
        if psf_b_key in self.names:
            self.psf_b_key = psf_b_key
        if pa_key in self.names:
            self.pa_key = pa_key


    def exclude(self, exclude_coords, exclusion_zone):
        """Exclude sources within the specified exclusion zone/s."""

        indices = []

        for i in range(len(self.coords)):

            excl_seps = self.coords[i].separation(exclude_coords)
            if (excl_seps.value < exclusion_zone).any():
                continue
            else:
                indices.append(i)

        self.table = self.table[indices]
        self.coords = self.coords[indices]


    def exclude_self(self, exclusion_zone):
        """Internal cross-match to avoid multiply-matched sources."""

        idx, seps, _ = self.coords.match_to_catalog_sky(self.coords, 
                                                        nthneighbor=2)

        indices = np.where(seps.value > exclusion_zone)[0]

        self.table = self.table[indices]
        self.coords = self.coords[indices]


    def only_within(self, coords, radius):
        """Remove sources outside of the radius specified."""

        sep = coords.separation(self.coords)

        self.table = self.table[np.where(sep.value < radius)]
        self.coords = self.coords[np.where(sep.value < radius)]


    def clip(self, threshold):
        """Clip sources based on Jy threshold."""

        if self.flux_key is not None:
            idx = np.where(self.table[self.flux_key]*self.flux_multiplier > threshold)[0]
            self.coords = self.coords[idx]
            self.table = self.table[idx]
        else:
            logger.warning("Thresholding is only possible if the Catalogue "
                           "object was initialised with a flux_key")


    def compact_by_resolution(self, a, b, bmaj, bmin, ratio=1.2):
        """Select only 'compact' sources."""

        if isinstance(bmaj, str):
            cond = np.where(((self.table[a]*self.table[b]) / 
                             (self.table[bmaj]*self.table[bmin])) < ratio)
        else:
            cond = np.where(((self.table[a]*self.table[b]) / 
                             (bmaj*bmin)) <  ratio)

        self.table = self.table[cond]
        self.coords = self.coords[cond]

    def compact_by_flux(self, int_flux, peak_flux, ratio=1.2):
        """Select only 'compact' sources."""

        cond = np.where((self.table[int_flux] / self.table[peak_flux]) < ratio)

        self.table = self.table[cond]
        self.coords = self.coords[cond]


    @staticmethod
    def open_catalogue(catalogue):
        if catalogue.endswith(".vot") or catalogue.endswith(".xml"):
            format = "votable"
        elif catalogue.endswith(".fits"):
            format = "fits"
        else:
            raise IOError("Catalogue file ({}) must be either a VOTable (.vot/.xml) "
                          "or a FITS table (.fits).".format(catalogue))
        
        table = Table.read(catalogue, format=format)
        return table


def match(cat1, cat2, separation, exclusion=0., include_duplicates=False):
    """Match two catalogues.

    Incorporates a minimum separation and an exclusion zone.
    """



    idx1, sep1, _ = cat1.coords.match_to_catalog_sky(cat2.coords)
    idx2, sep2, _ = cat1.coords.match_to_catalog_sky(cat2.coords,
                                                     nthneighbor=2)


    # if isinstance(separation, np.ndarray):
    #     separation = separation[idx1]
    # if isinstance(exclusion, np.ndarray):
    #     exclusion = exclusion[idx1]

    cond = np.where((sep1.value < separation) & (sep2.value > exclusion))[0]
    indices = np.array([cond, idx1[cond]]).T

    if len(idx1[cond]) == 0.:
        raise RuntimeError("No matches between {} and {}".format(cat1.name,
                                                                 cat2.name))
    
    if not include_duplicates:
        uniq = np.unique(indices[:, 1], return_counts=True)[1]
        indices = indices[np.where(uniq == 1)]

        duplicates = [uniq > 1]
        logger.debug("Removed {} duplicate matches between {} and {}".format(
                    len(duplicates), cat1.name, cat2.name))

    # indices[:, 0] --> cat1 indices
    # indices[:, 1] --> cat2 indices
    return indices 


def match_simple(coords1, coords2, separation, exclusion=0):

    idx1, sep1, _ = coords1.match_to_catalog_sky(coords2)
    idx2, sep2, _ = coords1.match_to_catalog_sky(coords2,
                                                     nthneighbor=2)

    cond = np.where((sep1.value < separation) & (sep2.value > exclusion))[0]
    indices = np.array([cond, idx1[cond]]).T

    if len(idx1[cond]) == 0.:
        raise RuntimeError("No matches!")

    uniq = np.unique(indices[:, 1], return_counts=True)[1]
    indices = indices[np.where(uniq == 1)]

    duplicates = [uniq > 1]
    logger.debug("Removed {} duplicate matches.".format(len(duplicates)))

    # indices[:, 0] --> cat1 indices
    # indices[:, 1] --> cat2 indices
    return indices 


def check_for_conflicts(cat2, cat1_keys=["flux", "eflux", "old_ra", "old_dec"]):
    """Check for column names that conflict.

    Output names for ``cat1`` will be: 
        * 'old_ra'
        * 'old_dec'
        * 'flux'
        * 'eflux'
        * 'local_rms'

    However we do not need to worry about 'local_rms' since we will ignore this.
    """
    for key in cat1_keys:
        if key in cat2.names:
            raise ValueError("{} cannot have the following keys: \n" + \
                             "* flux, eflux, old_ra, old_dec".format(cat2.name))


def write_out(cat1, cat2, indices, outname, nmax=100, 
              ):
    """Write out a catalogue of ``cat2`` coordinates with ``cat1`` appended.

    Parameters
    ----------
    cat1 : Catalogue object
        First/image catalogue.
    cat2 : Catalogue object
        Second/reference catalogue.
    indices : `~np.ndarray`
        2-D array of indices, where 1st dimension is indices in ``cat1`` and 
        second dimension is indices in ``cat2`` that corresponding to the 
        indices in ``cat1``. E.g., output from 
    outname : str
        Name of output FITS table file.
    nmax : int, optional
        Max number of sources in cross-matched catalogue. The actually number may
        be less than this limit. [Default 100]

    Note that Catalogue objects should be prepared earlier with relevant
    flux, eflux, and localrms keys if these columns are wanted.

    """

    check_for_conflicts(cat2)

    cut = slice(0, len(indices[:, 0]))
    if nmax is not None and cat1.flux_key is not None:
        if len(indices[:, 0]) > nmax:
        # threshold = round(np.sort(cat1.table[cat1.flux_key][indices[:, 0]])[::-1][nmax-1], 1)
        # cut = cat1.table[cat1.flux_key][indices[:, 0]] > threshold
            cut = np.argsort(cat1.table[cat1.flux_key][indices[:, 0]])[::-1][:nmax]

    logger.info("Sources in final catalogue: {}".format(
        len(cat1.table[cat1.ra_key][indices[:, 0]][cut])))


    outtable = cat2.table[indices[:, 1]][cut]
    outtable.add_column(cat1.table[cat1.ra_key][indices[:, 0]][cut], name="old_ra")
    outtable.add_column(cat1.table[cat1.dec_key][indices[:, 0]][cut], name="old_dec")
    if cat1.flux_key is not None:
        outtable.add_column(cat1.table[cat1.flux_key][indices[:, 0]][cut], name="flux")
    if cat1.eflux_key is not None:
        outtable.add_column(cat1.table[cat1.eflux_key][indices[:, 0]][cut], name="eflux")
    if cat1.localrms_key is not None:
        outtable.add_column(cat1.table[cat1.localrms_key][indices[:, 0]][cut], name="local_rms")
    if cat1.a_key is not None:
        outtable.add_column(cat1.table[cat1.a_key][indices[:, 0]][cut]/3600., name="major")
    if cat1.b_key is not None:
        outtable.add_column(cat1.table[cat1.b_key][indices[:, 0]][cut]/3600., name="minor")
    if cat1.pa_key is not None:
        outtable.add_column(cat1.table[cat1.pa_key][indices[:, 0]][cut], name="pangle")
    if cat1.psf_a_key is not None:
        outtable.add_column(cat1.table[cat1.psf_a_key][indices[:, 0]][cut], name="psf_major")
    if cat1.psf_b_key is not None:
        outtable.add_column(cat1.table[cat1.psf_b_key][indices[:, 0]][cut], name="psf_minor")



    if not outname.endswith(".fits"):
        # always write FITS tables - large votables take ages to load! 
        outname += ".fits"

    logger.info("writing cross-matched catalogue to {}".format(outname))
    outtable.write(outname, format="fits", overwrite=True)



def full_write_out(cat1, cat2, indices, outname, nmax=None):
    """Write out a fully merged cat1+cat2 catalogue. Columns have _1 and _2 appended.

    Parameters
    ----------
    cat1 : Catalogue object
        First/image catalogue.
    cat2 : Catalogue object
        Second/reference catalogue.
    indices : `~np.ndarray`
        2-D array of indices, where 1st dimension is indices in ``cat1`` and 
        second dimension is indices in ``cat2`` that corresponding to the 
        indices in ``cat1``. E.g., output from 
    outname : str
        Name of output FITS table file.
    nmax : int, optional
        Max number of sources in cross-matched catalogue. The actually number may
        be less than this limit. [Default no clip]

    """


    for col in cat1.names:  # iterate over non-changing dict
        cat1.table.rename_column(col, col+"_1")
    for col in cat2.names:
        cat2.table.rename_column(col, col+"_2")
    
    # there shouldn't really be conflicts, but who knows
    check_for_conflicts(cat2, cat1.table.columns)

    cut = slice(0, len(indices[:, 0]))
    if nmax is not None and cat1.flux_key is not None:
        if len(indices[:, 0]) > nmax:
            cut = np.argsort(cat1.table[cat1.flux_key+"_1"][indices[:, 0]])[::-1][:nmax]

    logger.info("Sources in final catalogue: {}".format(
        len(cat1.table[indices[:, 0]][cut])))

    outtable = cat1.table[indices[:, 0]][cut]
    for col in cat2.table.columns:
        outtable.add_column(
            col=cat2.table[col][indices[:, 1]][cut],
            name=col
        )

    if not outname.endswith(".fits"):
        outname += ".fits"

    logger.info("writing cross-matched catalogue to {}".format(outname))
    outtable.write(outname, format="fits", overwrite=True)


def individual_write_out(cat1, cat2, extension):
    """Write out individual clipped catalogues for testing purposes.
    
    Parameters
    ----------
    cat1 : Catalogue object
        First/image catalogue.
    cat2 : Catalogue object
        Second/reference catalogue.
    extension : str
        Added to intput catalogue names for clipped output names.
    """

    outname1 = ".".join(cat1.name.split(".")[:-1]) + "." + extension + ".fits"
    outname2 = ".".join(cat2.name.split(".")[:-1]) + "." + extension + ".fits"
    cat1.table.write(outname1, format="fits", overwrite=True)
    cat2.table.write(outname2, format="fits", overwrite=True)


# TODO
# def region_write_out(matched_cat, outname, color1="blue", color2="magenta",
#                      bmaj1=None, bmaj2=None):
#     """Write out a simple region file for DS9 to show the cross-matching."""

#     table = fits.open(matched_cat)[1].data

#     with open(outname, "w+") as f:
#         f.write("# Region file format: DS9 version 4.1\n")
#         f.write("global color=green dashlist=8 3 width=1 font=\"helvetica 10 normal roman\" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n")
#         f.write("fk5\n")


def match_catalogues(
    catalogue1,
    catalogue2,
    separation=None,
    key_separation=None,
    exclusion_zone=None,
    key_exclusion_zone=None,
    outname=None,
    threshold=0.,
    nmax=None,
    coords=None,
    radius=10.,
    full_merge=False,
    individual=None,
    ra1="ra",
    dec1="dec",
    ra2="ra",
    dec2="dec",
    flux_key="int_flux",
    eflux_key="err_int_flux",
    localrms_key="local_rms",
    beam1=None,
    beam_key1=[None, None],
    size1=[None, None],
    pa1=None,
    beam2=None,
    beam_key2=[None, None],
    size2=[None, None],
    flux_keys1=["int_flux", "peak_flux"],
    flux_keys2=["int_flux", "peak_flux"],
    flux_ratio=1.2,
    beam_ratio=-1,
    quiet=False,
    include_duplicates=False,
):
    """The pipeline to match catalogues along with pre-processing.
    
    
    
    """

    if quiet:
        logger.setLevel(logging.CRITICAL)

    logger.info("Opening first catalogue: {}".format(catalogue1))

    cat1 = Catalogue(
        catalogue1,
        ra_key=ra1,
        dec_key=dec1,
        flux_key=flux_key,
        eflux_key=eflux_key,
        localrms_key=localrms_key,
        a_key=size1[0],
        b_key=size1[1],
        psf_a_key=beam_key1[0],
        psf_b_key=beam_key1[1],
        pa_key=pa1
    )

    logger.info("Opening second catalogue: {}".format(catalogue2))

    cat2 = Catalogue(
        catalogue2,
        ra_key=ra2,
        dec_key=dec2
    )

    # Integrated/peak flux measurements for compactness test:
    if np.asarray([key in cat1.names for key in flux_keys1]).all() and \
        flux_ratio > 0.:

        logger.info("Excluding sources with S_int/S_peak > {} for {}".format(
            flux_ratio, cat1.name
        ))

        logger.debug("len(cat1) with all sources {}".format(len(cat1.table)))
        cat1.compact_by_flux(int_flux=flux_keys1[0],
                             peak_flux=flux_keys1[1],
                             ratio=flux_ratio)
        
        logger.debug("len(cat1) with compact sources {}".format(len(cat1.table)))
        
    if np.asarray([key in cat2.names for key in flux_keys2]).all() and \
        flux_ratio > 0.:

        logger.info("Excluding sources with S_int/S_peak > {} for {}".format(
            flux_ratio, cat2.name
        ))

        logger.debug("len(cat2) with all sources {}".format(len(cat2.table)))

        cat2.compact_by_flux(int_flux=flux_keys2[0],
                            peak_flux=flux_keys2[1],
                            ratio=flux_ratio)
        
        logger.debug("len(cat2) with compact sources {}".format(len(cat2.table)))

    # Additional compactness test based on source size relative to PSF:
    if (beam1 is not None or None not in beam_key1) \
        and (None not in size1) and (beam_ratio > 0.):

        logger.info("Excluding sources with a*b/psfa*psfb > {} for {}".format(
            beam_ratio, cat1.name
        ))

        logger.debug("len(cat1) with all sources {}".format(len(cat1.table)))

        if None not in beam_key1:
            beam1 = beam_key1

        cat1.compact_by_resolution(
            a=size1[0], b=size1[1], 
            bmaj=beam1[0], bmin=beam1[1], 
            ratio=beam_ratio
        )

        logger.debug("len(cat1) with compact sources {}".format(len(cat1.table)))

    if (beam2 is not None or beam_key2 is not None) \
        and (size2 is not None) and (beam_ratio > 0.):

        logger.info("Excluding sources with a*b/psfa*psfb > {} for {}".format(
            beam_ratio, cat2.name
        ))
        
        logger.debug("len(cat2) with all sources {}".format(len(cat2.table)))
        
        if beam_key2 is not None:
            beam2 = beam_key2

        cat2.compact_by_resolution(
            a=size2[0], b=size2[1], 
            bmaj=beam2[0], bmin=beam2[1], 
            ratio=beam_ratio
        )
        
        logger.debug("len(cat2) with compact sources {}".format(len(cat2.table)))

    if coords is not None:

        # Only consider sources within some radius around the specified coords:
        logger.info("Excluding sources outside of {} degrees around {}".format(
            radius, coords
        ))
        
        coords = SkyCoord(
            ra=coords[0], 
            dec=coords[1],
            unit=(u.deg, u.deg)
        )
        
        cat1.only_within(coords, radius)
        cat2.only_within(coords, radius)

    logger.info(
        "Clipping first catalogue based on threshold {}...".format(threshold)
    )
    cat1.clip(threshold)

    logger.info("Sources in first catalogue: {}".format(len(cat1.table)))

    if separation is None:
        separation = 1.e30


    if exclusion_zone > 0.:
        
        logger.info(
            "Excluding sources with neighbours within {} arcsec in the first catalogue".format(
                exclusion_zone*3600.
            )
        )
        
        cat1.exclude_self(exclusion_zone)

        logger.info(
            "Excluding sources with neighbours within {} arcsec in the second catalogue".format(
                exclusion_zone*3600.
            )
        )

        cat2.exclude_self(exclusion_zone)

    logger.info("Matching pre-processed catalogues...")

    if key_separation is not None:
        if separation is None:
            separation = 1.
        max_separation = np.asarray(cat1.table[key_separation] / separation)
    else:
        max_separation = separation
    if key_exclusion_zone is not None:
        if exclusion_zone is None:
            exclusion_zone = 1.
        max_zone = np.asarray(cat1.table[key_exclusion_zone] / exclusion_zone)
    else:
        max_zone = exclusion_zone

    try:
        indices = match(cat1, cat2, max_separation, max_zone, 
            include_duplicates=include_duplicates
        )
    except RuntimeError as e:
        logger.warning("No matches between {} and {} - exiting.".format(
            cat1.name, cat2.name
        ))
        return None

    logger.info("Total {} matches between {} and {}".format(
        len(indices), cat1.name, cat2.name
    ))

    if outname is None:

        outname = os.path.basename(catalogue1).split(".")[0] + "_" + \
                       os.path.basename(catalogue2).split(".")[0] + ".fits"
        logger.info("Setting output filename to {}".format(outname))

    
    if individual is not None:
        individual_write_out(cat1, cat2, individual)

    if full_merge:
        full_write_out(cat1, cat2, indices, outname, nmax=nmax)
    else:
        write_out(cat1, cat2, indices, outname, nmax=nmax)

    

def multi_match(catalogues, separation, outname,
    exclusion_zone=0.,
    ra_keys=[None],
    dec_keys=[None],
    int_flux_keys=[None],
    peak_flux_keys=[None],
    flux_ratio=-1,
    names=[None],
    keep_all_columns=False,
    ):
    """Progressively match multiple catalogues."""

    cats = []

    if len(ra_keys) != len(catalogues):
        logger.warning("Guessing RA keys for catalogues.")
        ra_keys = [None]*len(catalogues)
    if len(dec_keys) != len(catalogues):
        logger.warning("Guessing DEC keys for catalogues.")
        dec_keys = [None]*len(catalogues)
    if len(int_flux_keys) != len(catalogues):
        logger.warning("Guessing total fux keys for catalogues.")
        int_flux_keys = [None]*len(catalogues)
    if len(peak_flux_keys) != len(catalogues):
        logger.warning("Guessing peak flux keys for catalogues.")
        peak_flux_keys = [None]*len(catalogues)
    if len(names) != len(catalogues):
        names = ["{}".format(i) for i in range(len(catalogues))]

    for i, catalogue in enumerate(catalogues):

        logger.info("Opening {}".format(catalogue))
        
        cat = Catalogue(catalogue,
            ra_key=ra_keys[i],
            dec_key=dec_keys[i],
            flux_key=int_flux_keys[i],
            peak_flux_key=peak_flux_keys[i]
        )

        if not keep_all_columns:
            for col in cat.names:
                if col not in [cat.ra_key, cat.dec_key, cat.flux_key, cat.peak_flux_key]:
                    cat.table.remove_column(col)
            cat.names = cat.ra_key, cat.dec_key, cat.flux_key, cat.peak_flux_key



        if flux_ratio > 0:

            cat.compact_by_flux(
                int_flux=cat.flux_key,
                peak_flux=cat.peak_flux_key,
                ratio=flux_ratio
            )

        if exclusion_zone > 0:

            cat.exclude_self(exclusion_zone)

        cats.append(cat)


    prime_table = cats[0].table

    for i in range(len(cats)):
        print(cats[i].table.columns)

    for i in range(1, len(cats)):

        print(cats[i].table.columns)
        if not keep_all_columns:
            cats[i].table.rename_column(cats[i].ra_key, "ra_{}".format(names[i]))
            cats[i].table.rename_column(cats[i].dec_key, "dec_{}".format(names[i]))
            cats[i].table.rename_column(cats[i].flux_key, "int_flux_{}".format(names[i]))
            cats[i].table.rename_column(cats[i].peak_flux_key, "peak_flux_{}".format(names[i]))
        else:
            for col in cats[i].names:
                cats[i].table.rename_column(col, col+"_{}".format(names[i]))


        try:
            prime_coords = SkyCoord(
                ra=prime_table[cats[0].ra_key],
                dec=prime_table[cats[0].dec_key],
                unit=(u.deg, u.deg)
            )
        except Exception:
            prime_coords = SkyCoord(
                ra=prime_table[cats[0].ra_key],
                dec=prime_table[cats[0].dec_key],
            )

        indices = match_simple(
            coords1=prime_coords,
            coords2=cats[i].coords,
            separation=separation,
            exclusion=exclusion_zone
        )

        prime_table = prime_table[indices[:, 0]] 
        add_table = cats[i].table[indices[:, 1]]
        
        prime_table = hstack([prime_table, add_table], 
            table_names=[names[0], names[i]],
        )

    prime_table.write(outname, overwrite=True)


