import os
import logging
from argparse import HelpFormatter

# Misc. helpers for various parts of flux_warp will be initialised here.

# https://stackoverflow.com/questions/4519127/setuptools-package-data-folder-location
_ROOT = os.path.abspath(os.path.dirname(__file__))
_DEFAULT_MODEL_CATALOGUE = "GLEAM_EGC_params.fits"

OFFSET_M = "SNROFFM"
OFFSET_C = "SNROFFC"

# https://stackoverflow.com/a/23941599
class CustomFormatter(HelpFormatter):
    """Remove unnessary extra METAVAR entries in the help text."""
    def _format_action_invocation(self, action):
        if not action.option_strings:
            metavar, = self._metavar_formatter(action, action.dest)(1)
            return metavar
        else:
            parts = []
            # if the Optional doesn't take a value, format is:
            #    -s, --long
            if action.nargs == 0:
                parts.extend(action.option_strings)

            # if the Optional takes a value, format is:
            #    -s ARGS, --long ARGS
            # change to 
            #    -s, --long ARGS
            else:
                default = action.dest.upper()
                args_string = self._format_args(action, default)
                for option_string in action.option_strings:
                    parts.append("{}".format(option_string))
                parts[-1] += " {}".format(args_string)
            return ", ".join(parts)
    
def get_data(path):
    return os.path.join(_ROOT, "data", path)

